/////// zePierre
/////// MAIN FILE FOR RFC5_ATLAS



/////// GLOBAL VARIABLES

var mapMode = "";
var mapCivDDL = "";
var mapModeSpawnTilesOn = "";
var naturalWonders = "";
var cityStates = "";
var displayHexGrid = "";
var displayGraphicalOverlay = "";
var displayStabilityJunglesNotSettleable = "";
var tooltipMode = "";
var cityNamesDDL = "";
var opponentsOfCivDDL = "";

var MAJOR_CIVS_ORDERED = [];
sortMajorCivsByTurn();



/////// CONST

const ICONS_OVERLAY = ['resource_aluminum','resource_banana','resource_bison','resource_citrus','resource_coal','resource_cocoa','resource_copper','resource_cotton','resource_cow','resource_crab','resource_deer','resource_diamond','resource_dye','resource_fa_maize','resource_fa_rice','resource_fa_tea','resource_fa_tobacco','resource_fish','resource_fur','resource_gems','resource_gold','resource_horse','resource_incense','resource_iron','resource_ivory','resource_marble','resource_oil','resource_pearls','resource_salt','resource_sheep','resource_silk','resource_silver','resource_spices','resource_stone','resource_sugar','resource_truffles','resource_uranium','resource_whale','resource_wheat','resource_wine','terrain_forest','terrain_jungle','terrain_mountain'];

const HEX_COLOR = [];
HEX_COLOR["2"]                     = "forestgreen";
HEX_COLOR["-1"]                    = "yellowgreen";
HEX_COLOR["-2"]                    = "greenyellow";
HEX_COLOR["-3"]                    = "yellow";
HEX_COLOR["-5"]                    = "orange";
HEX_COLOR["-6"]                    = "orangered";
HEX_COLOR["-7"]                    = "red";
HEX_COLOR["-10"]                   = "darkred";
HEX_COLOR["city_state"]            = "darkorchid";
HEX_COLOR["city_state_new"]        = "red";
HEX_COLOR["coast"]                 = "rgb(150,183,243)";
HEX_COLOR["desert"]                = "rgb(188,188,148)";
HEX_COLOR["error"]                 = "black";
HEX_COLOR["flip"]                  = "red";
HEX_COLOR["flood_plains"]          = "rgb(143,205,135)";
HEX_COLOR["grass"]                 = "rgb(114,132,56)";
HEX_COLOR["jungle"]                = "rgb(0,64,0)";
HEX_COLOR["land_not_settleable"]   = "grey";
HEX_COLOR["marsh"]                 = "rgb(92,148,148)";
HEX_COLOR["minor_civ_capital"]     = "darkorchid";
HEX_COLOR["minor_civ_capital_new"] = "red";
HEX_COLOR["mountain"]              = "rgb(83,83,83)";
HEX_COLOR["natural_wonder"]        = "gold";
HEX_COLOR["no_start_civ"]          = "rgb(67,2,147)";
HEX_COLOR["oasis"]                 = "rgb(143,205,135)";
HEX_COLOR["ocean"]                 = "rgb(83,126,162)";
HEX_COLOR["plains"]                = "rgb(112,107,62)";
HEX_COLOR["snow"]                  = "ghostwhite";
HEX_COLOR["stability_undefined"]   = "white";
HEX_COLOR["start_civ"]             = "purple";
HEX_COLOR["tundra"]                = "lightgray";
HEX_COLOR["water_flip"]            = "darkred";

const HEX_CAPTIONS = [];
HEX_CAPTIONS["2"]                       = "Stability: +2";
HEX_CAPTIONS["-1"]                      = "Stability: -1";
HEX_CAPTIONS["-2"]                      = "Stability: -2";
HEX_CAPTIONS["-3"]                      = "Stability: -4";
HEX_CAPTIONS["-5"]                      = "Stability: -5";
HEX_CAPTIONS["-6"]                      = "Stability: -6";
HEX_CAPTIONS["-7"]                      = "Stability: -7";
HEX_CAPTIONS["-10"]                     = "Stability: -10";
HEX_CAPTIONS["city_state"]              = "City-State";
HEX_CAPTIONS["city_state_new"]          = "City-State appearing in this turn";
HEX_CAPTIONS["coast"]                   = "Terrain: coast";
HEX_CAPTIONS["desert"]                  = "Terrain: desert";
HEX_CAPTIONS["error"]                   = "Error loading tile";
HEX_CAPTIONS["flip"]                    = "Tile that will flip to the selected (re)spawning civ";
HEX_CAPTIONS["flood_plains"]            = "Terrain: flood plains";
HEX_CAPTIONS["grass"]                   = "Terrain: grass";
HEX_CAPTIONS["jungle"]                  = "Terrain: jungle (grass or plains)";
HEX_CAPTIONS["land_not_settleable"]     = "Stability: land that cannot be settled";
HEX_CAPTIONS["marsh"]                   = "Terrain: marsh";
HEX_CAPTIONS["minor_civ_capital"]       = "Minor civ capital";
HEX_CAPTIONS["minor_civ_capital_new"]   = "Minor civ capital that MAY appear from this turn";
HEX_CAPTIONS["mountain"]                = "Terrain: mountain";
HEX_CAPTIONS["natural_wonder"]          = "Natural wonder";
HEX_CAPTIONS["no_start_civ"]            = "Tile where a major civ will NOT start due to your civ selection";
HEX_CAPTIONS["oasis"]                   = "Terrain: oasis";
HEX_CAPTIONS["ocean"]                   = "Terrain: ocean";
HEX_CAPTIONS["plains"]                  = "Terrain: plains";
HEX_CAPTIONS["snow"]                    = "Terrain: snow";
HEX_CAPTIONS["stability_undefined"]     = "Stability: undefined (0)";
HEX_CAPTIONS["start_civ"]               = "Tile where a civ starts (hoover for name and year)";
HEX_CAPTIONS["tundra"]                  = "Terrain: tundra";
HEX_CAPTIONS["water_flip"]              = "Water tile that will flip to the selected (re)spawning civ";

const COLOR_RIVER = "blue";

const polygonPoints_x1 = 00;
const polygonPoints_y1 = 03;
const polygonPoints_x2 = 05;
const polygonPoints_y2 = 00;
const polygonPoints_x3 = 10;
const polygonPoints_y3 = 03;
const polygonPoints_x4 = 10;
const polygonPoints_y4 = 09;
const polygonPoints_x5 = 05;
const polygonPoints_y5 = 12;
const polygonPoints_x6 = 00;
const polygonPoints_y6 = 09;

const line_E_Points_x1 = 10;
const line_E_Points_y1 = 03;
const line_E_Points_x2 = 10;
const line_E_Points_y2 = 09;

const line_SE_Points_x1 = 05;
const line_SE_Points_y1 = 12;
const line_SE_Points_x2 = 10;
const line_SE_Points_y2 = 09;

const line_SW_Points_x1 = 00;
const line_SW_Points_y1 = 09;
const line_SW_Points_x2 = 05;
const line_SW_Points_y2 = 12;

const basicHexWidth = 10;
const basicHexHeight = 12;

const line_left_add_x = basicHexWidth / 2;
const line_add_y = 09;

// natural wonders that are not renamed by the mod
if (typeof NATURAL_WONDERS_RENAME["GIBRALTAR"] == "undefined")   NATURAL_WONDERS_RENAME["GIBRALTAR"]   = "Rock of Gibraltar";
if (typeof NATURAL_WONDERS_RENAME["MT_SINAI"] == "undefined")    NATURAL_WONDERS_RENAME["MT_SINAI"]    = "Mt. Sinai";
if (typeof NATURAL_WONDERS_RENAME["MT_KAILASH"] == "undefined")  NATURAL_WONDERS_RENAME["MT_KAILASH"]  = "Mt. Kailash";
if (typeof NATURAL_WONDERS_RENAME["KILIMANJARO"] == "undefined") NATURAL_WONDERS_RENAME["KILIMANJARO"] = "Mt. Kilimanjaro";
if (typeof NATURAL_WONDERS_RENAME["FUJI"] == "undefined")        NATURAL_WONDERS_RENAME["FUJI"]        = "Mt. Fuji";
if (typeof NATURAL_WONDERS_RENAME["REEF"] == "undefined")        NATURAL_WONDERS_RENAME["REEF"]        = "The Great Barrier Reef";
if (typeof NATURAL_WONDERS_RENAME["ULURU"] == "undefined")       NATURAL_WONDERS_RENAME["ULURU"]       = "Uluru";
if (typeof NATURAL_WONDERS_RENAME["POTOSI"] == "undefined")      NATURAL_WONDERS_RENAME["POTOSI"]      = "Cerro de Potosi";
if (typeof NATURAL_WONDERS_RENAME["CRATER"] == "undefined")      NATURAL_WONDERS_RENAME["CRATER"]      = "The Barringer Crater";
if (typeof NATURAL_WONDERS_RENAME["GEYSER"] == "undefined")      NATURAL_WONDERS_RENAME["GEYSER"]      = "Old Faithful";
if (typeof NATURAL_WONDERS_RENAME["SRI_PADA"] == "undefined")    NATURAL_WONDERS_RENAME["SRI_PADA"]    = "Sri Pada";
if (typeof NATURAL_WONDERS_RENAME["VOLCANO"] == "undefined")     NATURAL_WONDERS_RENAME["VOLCANO"]     = "Krakatoa";

// natural wonders that may cause encoding issues
NATURAL_WONDERS_RENAME["VOLCANO_PELEE"] = "Mt. Pelée";



/////// INIT PAGE

const MAP_BODY = document.getElementById("mapBody");
const zoomSlider = document.getElementById("zoomSlider");

// for low resolution, zoom out to the lowest value
if (window.innerWidth < 1400) {
	zoomSlider.value = 0;
}

initAllDDL();
initSettlerAdvisor();
initVictoryMenu();
initCityStatesSlider();
getDOMValues();
listOpponents();
buildMap();

tooltipTriggers = document.querySelectorAll('area');
Array.from(tooltipTriggers).map(trigger => {
	trigger.addEventListener('mousemove', showTooltip);
	trigger.addEventListener('mouseout', hideTooltip);
});




/////// RhyeDB FUNCTIONS

function RhyeCivNameToReadableName(rhye) {
	let name = rhye.substr("CIVILIZATION_".length);
	name = name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
	if (typeof MAJOR_CIVS_RENAME[rhye] !== "undefined") {
		name = MAJOR_CIVS_RENAME[rhye];
	}
	return name;
}

function RhyeMinorCivNameToReadableName(rhye) {
	let name = rhye.substr("MINOR_CIV_".length);
	name = name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
	if (typeof MINOR_CIVS_RENAME[rhye] !== "undefined") {
		name = MINOR_CIVS_RENAME[rhye];
	}
	return name;
}

function RhyeResourceNameToReadableName(technicalName) {
	let readableName = "";
	
	// instead of looking into <MOD_FOLDER>/AmerResources/Localization/AmerResources_en_US.xml
	if (technicalName.startsWith("FA_")) {
		readableName = technicalName.slice(3);
	} else {
		readableName = technicalName;
	}
	
	readableName = readableName[0].toUpperCase() + readableName.slice(1).toLowerCase();
	
	return readableName;
}

function findCivAttribute(civRhyeName, attribute) {
	y = 99999;
	MAJOR_CIVS.forEach(majorCiv => {
		if(civRhyeName == majorCiv["JSname"]) {
			y = majorCiv[attribute];
		}
	});
	return y;
}

function findSpawnYear(civRhyeName) {
	return findCivAttribute(civRhyeName,"SpawnYear");
}

function findSpawnTurn(civRhyeName) {
	return findCivAttribute(civRhyeName,"spawnTurn");
}

function findCityName(line, column, selectedCiv) {
	let name = ""; // default
	
	if (typeof MAP_CITY_NAMES[selectedCiv] !== 'undefined') {
		if (typeof MAP_CITY_NAMES[selectedCiv][countColumn*1000+countLine] !== 'undefined') {
			name = MAP_CITY_NAMES[selectedCiv][countColumn*1000+countLine];
		}
	}
	
	return name;
}

function sortMajorCivsByTurn() {
	i = 0;
	MAJOR_CIVS.forEach(majorCiv => {
		name = majorCiv["JSname"];
		turn = "" + ("00" + majorCiv["spawnTurn"]).slice(-3);
		MAJOR_CIVS_ORDERED[i] = [];
		MAJOR_CIVS_ORDERED[i][0] = turn;
		MAJOR_CIVS_ORDERED[i][1] = name;
		i++;
	});
	MAJOR_CIVS_ORDERED.sort((e1, e2) => e1[0] > e2[0] ? 1 : (e1[0] < e2[0] ? -1 : 0));
}

function formatYearIntoBCAD(year) {
	if (year < 0) {
		return -year + " BC";
	} else {
		return year + " AD";
	}
}



/////// DOM FUNCTIONS

function initAllDDL() {
    let options_cityNamesDDL = new Array();
    let options_mapCivDDL = new Array();
    let options_opponentsOfCivDDL = new Array();
	
	MAJOR_CIVS_ORDERED.forEach(majorCiv => {
		turn = majorCiv[0];
		name = majorCiv[1];
		
		optionNew = document.createElement("option");
		optionNew.value = name;
		optionNew.text = turn + " - " + RhyeCivNameToReadableName(name);
		options_cityNamesDDL.push(optionNew);
		
		optionNew = document.createElement("option");
		optionNew.value = name;
		optionNew.text = turn + " - " + RhyeCivNameToReadableName(name);
		options_mapCivDDL.push(optionNew);
		
		optionNew = document.createElement("option");
		optionNew.value = name;
		optionNew.text = "T" + turn + " - " + RhyeCivNameToReadableName(name);
		options_opponentsOfCivDDL.push(optionNew);
	});
	
    for (i = 0; i < MAJOR_CIVS.length; i++) {
        document.getElementById("cityNamesDDL").add(options_cityNamesDDL[i]);
        document.getElementById("mapCivDDL").add(options_mapCivDDL[i]);
        document.getElementById("opponentsOfCivDDL").add(options_opponentsOfCivDDL[i]);
	}
}

function initSettlerAdvisor() {
	document.getElementById("settlerAdvisorCivList").innerHTML = "";
	let list = "";
	MAJOR_CIVS_ORDERED.forEach(majorCiv => {
		rhyeCivName = majorCiv[1];
		list += "<li><a href='#' onclick=\"quickSetup_SettlerAdvisor('" + rhyeCivName + "')\"><img src='./resources/" + rhyeCivName + ".png' height='44' width='44' /><br>" + RhyeCivNameToReadableName(rhyeCivName) + "<br>T" + majorCiv[0] + " (" + TURN_TO_YEAR[parseInt(majorCiv[0])] + ")" + "</a></li>";
	});
	document.getElementById("settlerAdvisorCivList").innerHTML = list;
}

function initVictoryMenu() {
	list = document.getElementById('victoriesRPVList');
	list.innerHTML = "";
	liCollection = [];
	
	// creating li
	for (i=0; i<MAJOR_CIVS.length; i++) {
		var li = document.createElement('li');
		textLi = RhyeCivNameToReadableName(MAJOR_CIVS[i]["JSname"]) + ": " + MAJOR_CIVS[i]["RPVshortDesc"];
		
		// if a long description exists
		if (VICTORIES_RPV[MAJOR_CIVS[i]["JSname"]] != null) {
			var longDescription = VICTORIES_RPV[MAJOR_CIVS[i]["JSname"]];
			
			// reformat the text
			longDescription = longDescription.replace("[ICON_TEAM_2]","");
			longDescription = longDescription.replace("[ICON_TEAM_4]","");
			longDescription = longDescription.replace("[ICON_TEAM_4]","");
			longDescription = longDescription.split("[NEWLINE]");
			
			textLi += "<ul class='victoriesRPVDescriptionList'>";
			for (const lineDescription of longDescription) {
				textLi += "<li>" + lineDescription + "</li>";
			}
			textLi += "</ul>";
			
			/* TODO
			var ul = document.createElement('ul');
			//ul.classList.add("")
			for (const lineDescription of longDescription) {
				var liLineDescription = document.createElement('li');
				liLineDescription.textContent = lineDescription;
				ul.appendChild(liLineDescription);
			}
			li.appendChild(ul);
			*/
		}
		li.innerHTML = textLi;
		liCollection[liCollection.length] = li;
	}
	
	// sorting list TODO review this part
	liCollection.sort();
	
	// adding li
	for (i=0; i<liCollection.length; i++){
		list.appendChild(liCollection[i]);
	}
}

function initCityStatesSlider() {
	TIMELINE_CITY_STATES[TIMELINE_CITY_STATES.length] = TIMELINE_CITY_STATES[TIMELINE_CITY_STATES.length-1]+1;
	document.getElementById("cityStatesSlider").max = TIMELINE_CITY_STATES.length - 1;
}

function playAllCityStatesSlider() {
/*
	alert('start');
	while (document.getElementById("cityStatesSlider").value < document.getElementById("cityStatesSlider").max) {
		setTimeout(function () {
			increaseCityStatesSlider(1);
			buildMap();
		}, 1000); // 1 secondary
	}
	alert('end');
*/
	increaseCityStatesSlider(1);
	buildMap();
	setTimeout(function() {
		if (parseInt(document.getElementById("cityStatesSlider").value) < parseInt(document.getElementById("cityStatesSlider").max)) {
			playAllCityStatesSlider();
		}
	}, 1500);
}

function getDOMValues() {
	mapMode = document.querySelector('input[name="mapMode"]:checked').value;
	mapCivDDL = document.getElementById("mapCivDDL").value;
	opponentsOfCivDDL = document.getElementById("opponentsOfCivDDL").value;
	mapModeSpawnTilesOn = document.getElementById("spawnTiles").checked;
	mapModeCityStatesOn = document.getElementById("cityStates").checked;
	mapModeCityStatesSliderValue = document.getElementById("cityStatesSlider").value;
	mapModeNaturalWondersOn = document.getElementById("naturalWonders").checked;
	displayHexGrid = document.getElementById("displayHexGrid").checked;
	displayGraphicalOverlay = document.getElementById("displayGraphicalOverlay").checked;
	displayStabilityJunglesNotSettleable = document.getElementById("displayStabilityJunglesNotSettleable").checked;
	tooltipMode = document.querySelector('input[name="tooltipMode"]:checked').value;
	cityNamesDDL = document.getElementById("cityNamesDDL").value;
}

function updateMenuFormAndHeader() {
	getDOMValues();
	
	// spanStabilityJungles visible only with mapMode stability
	if (mapCivDDL != "" && mapMode == 'stability') {
		document.getElementById("spanStabilityJungles").classList.remove("hidden");
	} else {
		document.getElementById("spanStabilityJungles").classList.add("hidden");
	}
	
	// city states checkbox makes its slider and the timeline indicator appear or disappear
	if (document.getElementById("cityStates").checked) {
		document.getElementById("cityStatesSliderContainer").classList.remove("hidden");
		document.getElementById("turnYearDisplay").classList.remove("hidden");
		document.getElementById("turnYearDisplay_turn").innerHTML = TIMELINE_CITY_STATES[mapModeCityStatesSliderValue];
		document.getElementById("turnYearDisplay_year").innerHTML = formatYearIntoBCAD(TURN_TO_YEAR[TIMELINE_CITY_STATES[mapModeCityStatesSliderValue]]);
	} else {
		document.getElementById("cityStatesSliderContainer").classList.add("hidden");
		document.getElementById("turnYearDisplay").classList.add("hidden");
	}
	
	// stability DDL makes stability radio buttons clickable or not
	document.getElementById("mapMode_stability").disabled = (mapCivDDL == "");
	document.getElementById("mapMode_flipArea").disabled = (mapCivDDL == "");
	document.getElementById("mapMode_respawnFlipArea").disabled = (mapCivDDL == "");
	if (mapCivDDL == "") {
		document.getElementById("mapCivDDL_TextAfter").innerHTML = "";
	} else {
		document.getElementById("mapCivDDL_TextAfter").innerHTML = " should display its:";
	}
	
	// cityNamesDDL can be modified only if the tooltip selected mode is to display city names
	document.getElementById("cityNamesDDL").disabled = (tooltipMode != "cityNames");
}

function increaseCityStatesSlider(step) {
	document.getElementById('cityStatesSlider').value = Math.round(document.getElementById('cityStatesSlider').value) + step;
}

function download_SVG() {
	let downloadAllowed = true;
	if (displayGraphicalOverlay) {
		downloadAllowed = false;
		if (confirm('The atlas needs to disable Game icons before download. Proceed?')) {
			downloadAllowed = true;
			document.getElementById("displayGraphicalOverlay").checked = false;
			buildMap();
		}
		//TODO: always store icons on Internet? relative paths to the icons will break the display of the whole image
	}
	if (downloadAllowed) {
		var e = document.createElement('a');
		e.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent('<?xml version="1.0" standalone="no"?>' + svgMap.split('&nbsp;').join(' ').split('<br>----------------<br>').join(': ')));
		
		filename = "";
		if (mapCivDDL != "") {
			filename = filename + RhyeCivNameToReadableName(mapCivDDL) + "_" + mapMode.substring(0,4) + "_";
		}
		if (cityNamesDDL != "" && cityNamesDDL != mapCivDDL) {
			filename = filename + RhyeCivNameToReadableName(cityNamesDDL) + "_cities_";
		}
		// remove last "_"
		if (filename.length > 0) {
			filename = filename.substring(0, filename.length-1);
		} else {
			filename = "map";
		}
		filename = filename + ".svg";
		
		
		e.setAttribute('download', filename);
		e.style.display = 'none';
		document.body.appendChild(e);
		e.click();
		document.body.removeChild(e);
	}
}

function showTooltip(event) {
	let element = event.target;
	let tooltipElement = document.getElementById('tooltip');
	let title;

	if (!element.dataset.title) {
		titleElement = element.querySelector('title');
		if (titleElement != null) {
			title = titleElement.innerHTML;
			event.target.setAttribute('data-title', title);
			titleElement.parentNode.removeChild(titleElement);
		} else {
			title = element.getAttribute('alt');
			if (titleElement != null) {
				event.target.setAttribute('data-title', title);
			}
		}
	} else {
		title = element.dataset.title;
	}
	
	// if there is something to display
	if (title != null) {
		tooltipElement.innerHTML = title;
		tooltipElement.style.display = 'block';
		if (event.screenX < window.innerWidth/2) {
			tooltipElement.style.left = event.pageX + 35 + 'px';
			tooltipElement.style.right = null;
		} else {
			// if the mouse reached half the horizontal axis, the tooltip moves to the left side of the mouse
			tooltipElement.style.left = null;
			tooltipElement.style.right = window.innerWidth - event.pageX + 5 + 'px';
		}
		let nbLines = (title.match(/<br>/g) || []).length + 1;
		if (event.screenY + 10 + 25*nbLines < window.innerHeight) {
			tooltipElement.style.top = event.pageY + 10 + 'px';
			tooltipElement.style.bottom = null;
		} else {
			// if the mouse reached the bottom of the screen
			tooltipElement.style.top = null;
			tooltipElement.style.bottom = window.innerHeight - event.pageY - 10 + 'px';
		}
		let longestString = (arrStr) => {
			let longest = '';
			arrStr.forEach((item) => {
				if (item.length >longest.length) {
					longest = item;
				}
			});
			return longest;
		}
		longestLineNbChar = longestString(tooltipElement.innerHTML.split("<br>")).length;
		let max = window.innerWidth - event.pageX - 10;
		let min = Math.min(longestLineNbChar *7, max);
	}
}

function hideTooltip() {
	var tooltip = document.getElementById('tooltip');
	tooltip.style.display = 'none';
}

function page_bar_mousehover(elementHovered) {
	switch (elementHovered) {
		case "tooltips_choices":
			document.getElementById('page_bottom_right_bar').src='./resources/page_bottom_right_bar_hover_tooltips.png';
			break;
		case "hexes_choices":
			document.getElementById('page_bottom_right_bar').src='./resources/page_bottom_right_bar_hover_hexes.png';
			break;
	}
}

function page_bar_mouseout() {
	document.getElementById('page_bottom_right_bar').src='./resources/page_bottom_right_bar.png';
}

function listOpponents() {
	list = document.getElementById('opponentsOfCivList');
	list.innerHTML = "";
	liCollection = [];
	
	// creating li
	for (i=0; i<PARTICIPANTS_LIST.length; i++) {
		var li = document.createElement('li');
		if (opponentsOfCivDDL != "" && opponentsOfCivDDL != PARTICIPANTS_LIST[i]) {
			if (typeof PARTICIPANTS_VERSUS[opponentsOfCivDDL][PARTICIPANTS_LIST[i]] != "undefined") {
				if (PARTICIPANTS_VERSUS[opponentsOfCivDDL][PARTICIPANTS_LIST[i]] == 100) {
					li.setAttribute('class','ok');
				} else if (PARTICIPANTS_VERSUS[opponentsOfCivDDL][PARTICIPANTS_LIST[i]] == 50) {
					li.setAttribute('class','maybe');
				} else if (PARTICIPANTS_VERSUS[opponentsOfCivDDL][PARTICIPANTS_LIST[i]] == 0) {
					li.setAttribute('class','ko');
				}
			}
		}
		li.setAttribute('value',RhyeCivNameToReadableName(PARTICIPANTS_LIST[i]));
		li.textContent = RhyeCivNameToReadableName(PARTICIPANTS_LIST[i]);
		liCollection[liCollection.length] = li;
	}
	
	// sorting list
	liCollection.sort(function(a, b){return a.innerHTML - b.innerHTML});
	
	// adding li
	for (i=0; i<liCollection.length; i++){
		list.appendChild(liCollection[i]);
	}
}

function quickSetup_SettlerAdvisor(rhyeCivName) {
	idsDDL = ['cityNamesDDL', 'mapCivDDL', 'opponentsOfCivDDL'];
	idsDDL.forEach(idDDL => {
		var ddl = document.getElementById(idDDL);
		var opts = ddl.options.length;
		for (var i=0; i<opts; i++){
			if (ddl.options[i].value == rhyeCivName){
				ddl.options[i].selected = true;
			} else {
				ddl.options[i].selected = false;
			}
		}
	});
	document.querySelectorAll('input[name="tooltipMode"]').forEach(radiobtn => {
		radiobtn.checked = false;
	});
	document.getElementById("tooltipMode_flipRisk").checked = true;
	buildMap();
}



/////// SVG FUNCTIONS

function getZoomMultiplier() {
	multiplier = Math.round(zoomSlider.value);
	if (multiplier == 0) {
		multiplier = 0.7;
	}
	return multiplier;
}

function SVG_river_E(_x0, _y0) {
	return SVG_river(_x0, _y0, line_E_Points_x1, line_E_Points_y1, line_E_Points_x2, line_E_Points_y2);
}

function SVG_river_SW(_x0, _y0) {
	return SVG_river(_x0, _y0, line_SW_Points_x1, line_SW_Points_y1, line_SW_Points_x2, line_SW_Points_y2);
}

function SVG_river_SE(_x0, _y0) {
	return SVG_river(_x0, _y0, line_SE_Points_x1, line_SE_Points_y1, line_SE_Points_x2, line_SE_Points_y2);
}

function SVG_river(_x0, _y0, _x1, _y1, _x2, _y2) {
	x1 = _x0 + _x1*getZoomMultiplier();
	y1 = _y0 + _y1*getZoomMultiplier();
	x2 = _x0 + _x2*getZoomMultiplier();
	y2 = _y0 + _y2*getZoomMultiplier();
	let line1 = "<line x1=\"" +  x1 + "\" y1=\"" + y1 + "\" x2=\"" + x2 + "\" y2=\"" + y2 + "\" stroke-width=\"" + getZoomMultiplier() + "\"/>";
	let radiusCircles = Math.round((getZoomMultiplier()-1)/2);
	//radiusCircles = radiusCircles < 1 ? radiusCircles : radiusCircles;
	let circle1 = '<circle cx="' + x1 + '" cy="' + y1 + '" r="' + radiusCircles + '"></circle>';
	let circle2 = '<circle cx="' + x2 + '" cy="' + y2 + '" r="' + radiusCircles + '"></circle>';
	return circle1 + line1 + circle2;
}

function SVG_polygon(x0, y0, color, border, tooltip) {
	let polygon = "<polygon points=\"";
	
	polygon += (x0 + polygonPoints_x1*getZoomMultiplier()) + ",";
	polygon += (y0 + polygonPoints_y1*getZoomMultiplier()) + " ";
	
	polygon += (x0 + polygonPoints_x2*getZoomMultiplier()) + ",";
	polygon += (y0 + polygonPoints_y2*getZoomMultiplier()) + " ";
	
	polygon += (x0 + polygonPoints_x3*getZoomMultiplier()) + ",";
	polygon += (y0 + polygonPoints_y3*getZoomMultiplier()) + " ";
	
	polygon += (x0 + polygonPoints_x4*getZoomMultiplier()) + ",";
	polygon += (y0 + polygonPoints_y4*getZoomMultiplier()) + " ";
	
	polygon += (x0 + polygonPoints_x5*getZoomMultiplier()) + ",";
	polygon += (y0 + polygonPoints_y5*getZoomMultiplier()) + " ";
	
	polygon += (x0 + polygonPoints_x6*getZoomMultiplier()) + ",";
	polygon += (y0 + polygonPoints_y6*getZoomMultiplier()) + "";
	
	polygon += "\" fill=\"" + color + "\"";
	
	polygon += " stroke=\"";
	if (border) {
		polygon += "black" + "\" stroke-width=\"" + (getZoomMultiplier()/5) + "\"";
	} else {
		polygon += color + "\" stroke-width=\"1\"";
	}
	
	if (tooltip == "") {
		polygon += "/>";
	} else {
		let tooltipStart = "";
		switch (tooltipMode) {
			case "tileResource":
				tooltipStart = "Tile resource";
				break;
			case "flipRisk":
				tooltipStart = "Flip risk";
				break;
			case "cityNames":
				tooltipStart = "City names of " + RhyeCivNameToReadableName(cityNamesDDL);
				break;
			case "coordinatesForEachHex":
				tooltipStart = "Coordinates x;y";
				break;
			case "hexColorDescription":
				tooltipStart = "Hex color description";
				break;
			case "":
				tooltipStart = "Hex color description";
				//TODO: tooltip is messy, it can have several reasons (e.g. Paris & Lutetia) and it's hard to track the reason here; may be better to create a function to append a line to the tooltip
				break;
		}
		tooltipStart = tooltipStart.toUpperCase() + "<br>----------------<br>";
		polygon += "><title>" + tooltipStart + tooltip + "</title></polygon>";
	}
	
	return polygon;
}



/////// BUILD MAP

function generateTooltipResource(countColumn, countLine) {
	tooltip = "";
	
	// map resource
	if (MAP[countLine][countColumn][4] != 0) {
		tooltip = RhyeResourceNameToReadableName(MAP[countLine][countColumn][4]);
	
	// resource that appears later
	} else if (typeof MAP_COLUMBIAN_EXCHANGE[countColumn*1000+countLine] != "undefined") {
		tooltip = "From 17th century: " + RhyeResourceNameToReadableName(MAP_COLUMBIAN_EXCHANGE[countColumn*1000+countLine]["resource"]);
	}
	return tooltip;
}

function buildMap() {
	getDOMValues();
	updateMenuFormAndHeader();
	listOpponents();
	MAP_BODY.innerHTML = "";
			
	let svgRivers = "";
	
	if (mapCivDDL != "") {
		civFlipAreaNorthY = findCivAttribute(mapCivDDL,"FlipAreaNorthY");
		civFlipAreaSouthY = findCivAttribute(mapCivDDL,"FlipAreaSouthY");
		civFlipAreaWestX = findCivAttribute(mapCivDDL,"FlipAreaWestX");
		civFlipAreaEastX = findCivAttribute(mapCivDDL,"FlipAreaEastX");
		civRespawnFlipAreaNorthY = findCivAttribute(mapCivDDL,"RespawnFlipAreaNorthY");
		civRespawnFlipAreaSouthY = findCivAttribute(mapCivDDL,"RespawnFlipAreaSouthY");
		civRespawnFlipAreaWestX = findCivAttribute(mapCivDDL,"RespawnFlipAreaWestX");
		civRespawnFlipAreaEastX = findCivAttribute(mapCivDDL,"RespawnFlipAreaEastX");
	}
	
	// init svg map
	svgMap = "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" width=\"" + basicHexWidth*getZoomMultiplier()*(MAP_NB_COLUMNS+0.5) + "\" height=\"" + (basicHexHeight+(line_add_y*MAP_NB_LINE))*getZoomMultiplier() + "\">";
	imgOverlayIconWidth = 10 * getZoomMultiplier();
	imgOverlayIconHeight = 12 * getZoomMultiplier();
	svgMap += '<defs>';
	svgMap += '<style type="text/css"><![CDATA[';
	if (displayGraphicalOverlay) {
		ICONS_OVERLAY.forEach(icon => {
			svgMap += ' .iconOverlay_' + icon + ' > polygon:last-child { fill: url(#imgOverlay_' + icon + '); }';
		});
	}
	svgMap += ' .rivers { fill: ' + COLOR_RIVER + '; stroke: ' + COLOR_RIVER + '}';
	svgMap += ']]></style>';
	if (displayGraphicalOverlay) {
		ICONS_OVERLAY.forEach(icon => {
			svgMap += '<pattern id="imgOverlay_' + icon + '" x="0" y="0" width="1" height="1" viewBox="0 0 ' + imgOverlayIconWidth + ' ' + imgOverlayIconHeight + '" preserveAspectRatio="xMidYMid slice">'
				+ '<image width="' + imgOverlayIconWidth + '" height="' + imgOverlayIconHeight + '" xlink:href="./resources/' + icon + '.png"/>'
				+ '</pattern>';
		});
	}
	svgMap += '</defs>';
	
	
	
	// we go through each line, starting from the bottom
	for(countLine = 0; countLine < MAP_NB_LINE; countLine++) {
		
		// the top left corner of the line and of the tile (y coordinate)
		tile_y0 = (MAP_NB_LINE-countLine-1)*line_add_y*getZoomMultiplier();
		
		
		
		// column (hex) creation
		for(countColumn = 0; countColumn < MAP_NB_COLUMNS; countColumn++) {
			// within this loop, the "Rhye id" of the hex can be calculated as countColumn*1000+countLine
			
			// the top left corner of the column and of the tile (x coordinate)
			tile_x0 = (countColumn)*basicHexWidth*getZoomMultiplier() + (countLine%2)*line_left_add_x*getZoomMultiplier();
			
			let tile = MAP[countLine][countColumn];
			let terrain = tile[0];
			let elevation = tile[1];
			let feature1 = tile[2];
			let naturalWonderMapName = tile[3];
			let rivers = tile[5];
			
			let displayIconOverlay = "";
			
			
			
			///// --- COLOR TILE & TOOLTIP ---
			
			let hexColorDescribes = "error"; // default
			let hexTooltip = "";
			
			///// 1- NATURAL WONDER / MOUNTAIN (color overrides all)
			
			if (mapModeNaturalWondersOn && naturalWonderMapName != "") {
				hexColorDescribes = "natural_wonder";
				if (typeof NATURAL_WONDERS_RENAME[naturalWonderMapName] !== "undefined") {
					hexTooltip += NATURAL_WONDERS_RENAME[naturalWonderMapName];
				} else {
					hexTooltip += naturalWonderMapName;
				}
			}
			else if (elevation == "m") {
				hexColorDescribes = "mountain";
			}
			else {
			
				///// 2- TERRAIN color
				// terrain values: OCEAN, COAST, SNOW, TUNDRA, GRASS, PLAINS, DESERT
				switch (terrain) {
					case "O": hexColorDescribes = "ocean"; break;
					case "C": hexColorDescribes = "coast"; break;
					case "S": hexColorDescribes = "snow"; break;
					case "T": hexColorDescribes = "tundra"; break;
					case "G": hexColorDescribes = "grass"; break;
					case "P": hexColorDescribes = "plains"; break;
					case "D": hexColorDescribes = "desert"; break;
					default : hexColorDescribes = "error"; break;
				}
				
				// feature marsh and jungle override terrain
				if (feature1 == "MARSH" || feature1 == "JUNGLE" || feature1 == "FLOOD_PLAINS" || feature1 == "OASIS") {
					hexColorDescribes = feature1.toLowerCase();
				}
				
				///// 3- MODE SELECTED FOR MAP
				
				if (mapCivDDL != "" && mapMode != "") {
					
					///// 3a- stability
					if (mapMode == 'stability') {
						// terrains are erased (but not mountains nor water)
						if (terrain != "O" && terrain != "C") {
							hexColorDescribes = "stability_undefined";
						}
						
						// snow, tundra, marsh and desert (without flood plains) cannot be settled
						if (terrain == "S" || terrain == "T" || feature1 == "MARSH" || (terrain == "D" && feature1 != "FLOOD_PLAINS" && feature1 != "OASIS")) {
							hexColorDescribes = "land_not_settleable";
						}
						
						// we consider all natural wonders as not settleable
						if (naturalWonderMapName != "") {
							hexColorDescribes = "land_not_settleable";
						}
						
						if (displayStabilityJunglesNotSettleable && feature1 == "JUNGLE") {
							hexColorDescribes = "land_not_settleable";
						}
						
						// the Huns for example have no stability map defined
						if (typeof MAP_Stability[mapCivDDL] !== "undefined") {
							
							// stability values: 2; -1; -2; -3; -5; -6; -7; -10
							if (MAP_Stability[mapCivDDL][countColumn*1000+countLine]) {
								hexColorDescribes = "" + MAP_Stability[mapCivDDL][countColumn*1000+countLine];
							}
						}
					}
					///// 3b- flipArea
					else if (mapMode == 'flipArea') {
						if (( (civFlipAreaWestX <= countColumn) && (countColumn <= civFlipAreaEastX) && (civFlipAreaSouthY <= countLine) && (countLine <= civFlipAreaNorthY) && (typeof MAP_FlipAreaSubtractPlots[mapCivDDL][countColumn] == 'undefined' || MAP_FlipAreaSubtractPlots[mapCivDDL][countColumn][countLine] != true)) || (typeof MAP_FlipAreaAdditionalPlots[mapCivDDL][countColumn] !== 'undefined' && MAP_FlipAreaAdditionalPlots[mapCivDDL][countColumn][countLine] == true)) {
							hexColorDescribes = "flip";
							if (terrain == "O" || terrain == "C") {
								hexColorDescribes = "water_flip";
							}
						}
					}
					///// 3c- respawnFlipArea
					else if (mapMode == 'respawnFlipArea') {
						if (( (civRespawnFlipAreaWestX <= countColumn) && (countColumn <= civRespawnFlipAreaEastX) && (civRespawnFlipAreaSouthY <= countLine) && (countLine <= civRespawnFlipAreaNorthY) && (typeof MAP_RespawnFlipAreaSubtractPlots[mapCivDDL][countColumn] == 'undefined' || MAP_RespawnFlipAreaSubtractPlots[mapCivDDL][countColumn][countLine] != true)) || (typeof MAP_RespawnFlipAreaAdditionalPlots[mapCivDDL][countColumn] !== 'undefined' && MAP_RespawnFlipAreaAdditionalPlots[mapCivDDL][countColumn][countLine] == true)) {
							hexColorDescribes = "flip";
							if (terrain == "O" || terrain == "C") {
								hexColorDescribes = "water_flip";
							}
						}
					}
				}
				
				///// 4- SPAWN TILES (may override one of the map modes)
				if (mapModeSpawnTilesOn) {
					if (typeof MAP_START_CIV[countColumn] !== 'undefined' && typeof MAP_START_CIV[countColumn][countLine] !== 'undefined') {
						hexColorDescribes = "start_civ";
						
						// override tooltip, even if city name
						civRhyeName = MAP_START_CIV[countColumn][countLine];
						spawnYear = civRhyeName;
						hexTooltip += RhyeCivNameToReadableName(civRhyeName) + " appears in " + findSpawnYear(civRhyeName) + " (T" + findSpawnTurn(civRhyeName) + ")";
						
						if (opponentsOfCivDDL != "" && opponentsOfCivDDL != civRhyeName) {
							if (typeof PARTICIPANTS_VERSUS[opponentsOfCivDDL][civRhyeName] != "undefined") {
								if (PARTICIPANTS_VERSUS[opponentsOfCivDDL][civRhyeName] == 50) {
									hexColorDescribes = "start_civ";
									if (findSpawnYear(civRhyeName) > findSpawnYear(opponentsOfCivDDL)) {
										hexTooltip += RhyeCivNameToReadableName(civRhyeName) + " may appear in " + findSpawnYear(civRhyeName) + " (T" + findSpawnTurn(civRhyeName) + ")";
									} else {
										hexTooltip += RhyeCivNameToReadableName(civRhyeName) + " may respawn";
									}
								} else if (PARTICIPANTS_VERSUS[opponentsOfCivDDL][civRhyeName] == 0) {
									hexColorDescribes = "no_start_civ";
									hexTooltip = RhyeCivNameToReadableName(civRhyeName) + " will not appear if you play " + RhyeCivNameToReadableName(opponentsOfCivDDL);
								}
							}
						}
					}
					else if (typeof MAP_SECONDARY_START_CIV[countColumn] !== 'undefined' && typeof MAP_SECONDARY_START_CIV[countColumn][countLine] !== 'undefined') {
						hexColorDescribes = "start_civ";
						
						// override tooltip, even if city name
						civRhyeName = MAP_SECONDARY_START_CIV[countColumn][countLine];
						spawnYear = civRhyeName;
						hexTooltip += RhyeCivNameToReadableName(civRhyeName) + " (secondary start)";
					}
				}
				
				
				
				///// 5- CITY STATES
				
				// we will display city states only if the user has selected this mode in the menu
				if (mapModeCityStatesOn) {
					let displayCityState = false;
					let displayCapital = false;
					
					// if there is a city state on that tile
					if (typeof MAP_CITY_STATES[countColumn*1000+countLine] != "undefined") {
						
						// manage mapModeCityStatesSliderValue
						if (TIMELINE_CITY_STATES[mapModeCityStatesSliderValue] >= MAP_CITY_STATES[countColumn*1000+countLine]["StartTurn"]) {
							
							// AND if an opponentsOfCivDDL is selected, it needs to NOT match MajorCivReplacement NOR MajorCivReplacement2
							if ((typeof MAP_CITY_STATES[countColumn*1000+countLine]["MajorCivReplacement"] == "undefined" || MAP_CITY_STATES[countColumn*1000+countLine]["MajorCivReplacement"] != opponentsOfCivDDL) && (typeof MAP_CITY_STATES[countColumn*1000+countLine]["MajorCivReplacement2"] == "undefined" || MAP_CITY_STATES[countColumn*1000+countLine]["MajorCivReplacement2"] != opponentsOfCivDDL)) {
								displayCityState = true;
								minorCivTechnicalName = MAP_CITY_STATES[countColumn*1000+countLine]["CivType"];
								minorCivNo = MINOR_CIVS_NUMBER[minorCivTechnicalName];
								cityName = MAP_CITY_STATES[countColumn*1000+countLine]["CityName"];
							}
						}
					}
					// OR if there is a capital of a minor civ on that tile
					else if (typeof MAP_MINOR_CIVS_CAPITALS[countColumn*1000+countLine] != "undefined") {
						minorCivTechnicalName = MAP_MINOR_CIVS_CAPITALS[countColumn*1000+countLine];
						minorCivNo = MINOR_CIVS_NUMBER[minorCivTechnicalName];
						
						// manage mapModeCityStatesSliderValue
						if (TIMELINE_CITY_STATES[mapModeCityStatesSliderValue] >= MINOR_CIVS[minorCivNo]["MinTurn"]) {
							displayCapital = true;
							cityName = MINOR_CIVS[minorCivNo]["CapitalName"];
						}
					}
					
					if (displayCityState || displayCapital) {
						
						// if an opponentsOfCivDDL is selected, it needs to NOT match NotOverlap of that minor Civ
						if (opponentsOfCivDDL == "" || typeof MINOR_CIVS[minorCivNo]["NotOverlap"] == "undefined" || MINOR_CIVS[minorCivNo]["NotOverlap"] != opponentsOfCivDDL) {
							
							// we can display a different hex color depending on minor civ capital or city state
							
							if (displayCapital) {
								hexColorDescribes = "minor_civ_capital";
								// it just appeared
								if (TIMELINE_CITY_STATES[mapModeCityStatesSliderValue] == MINOR_CIVS[minorCivNo]["MinTurn"]) {
									hexColorDescribes = "minor_civ_capital_new";
								}
							} else if (displayCityState) {
								hexColorDescribes = "city_state";
								// it just appeared
								if (TIMELINE_CITY_STATES[mapModeCityStatesSliderValue] == MAP_CITY_STATES[countColumn*1000+countLine]["StartTurn"]) {
									hexColorDescribes = "city_state_new";
								}
							}
							
							// in the tooltip: we add minor civ name, years, whether it's a capital of a minor civ, a replacement of a major civ, and hostility vs information
							
							minorCivName = RhyeMinorCivNameToReadableName(minorCivTechnicalName);
							minorCivMinYear = TURN_TO_YEAR[MINOR_CIVS[minorCivNo]["MinTurn"]];
							minorCivMaxYear = TURN_TO_YEAR[MINOR_CIVS[minorCivNo]["MaxTurn"]];
							
							hexTooltip += cityName + " (";
							if (displayCapital) {
								hexTooltip += "capital of ";
							}
							hexTooltip += minorCivName;
							
							if (typeof minorCivMinYear != "undefined") {
								hexTooltip += ": " + minorCivMinYear + "-" + minorCivMaxYear;
							}
							
							if (displayCityState) {
								if (typeof MAP_CITY_STATES[countColumn*1000+countLine]["MajorCivReplacement"] != "undefined") {
									hexTooltip += ", replacement of " + RhyeCivNameToReadableName(MAP_CITY_STATES[countColumn*1000+countLine]["MajorCivReplacement"]);
								}
								
								if (typeof MAP_CITY_STATES[countColumn*1000+countLine]["MajorCivReplacement2"] != "undefined") {
									hexTooltip += " and " + RhyeCivNameToReadableName(MAP_CITY_STATES[countColumn*1000+countLine]["MajorCivReplacement2"]);
								}
							}
							
							if (MINOR_CIVS[minorCivNo]["HostileVsAll"]) {
								hexTooltip += ", hostile to all";
							}
							if (opponentsOfCivDDL != "" && typeof MINOR_CIVS[minorCivNo]["HostileVs"] != "undefined" && MINOR_CIVS[minorCivNo]["HostileVs"].includes(opponentsOfCivDDL)) {
								hexTooltip += ", hostile to " + RhyeCivNameToReadableName(opponentsOfCivDDL);
							}
							
							hexTooltip += ")";
						}
					}
				}
			}
			
			
			hexColorDescribes = hexColorDescribes + "";
			hexColorDescribes = hexColorDescribes.toLowerCase();
			let hexColor = "black";
			
			if (typeof HEX_COLOR[hexColorDescribes] !== "undefined") {
				hexColor = HEX_COLOR[hexColorDescribes];
			}
			
			
			
			///// TOOLTIP MODES
			
			///// tooltip: city name
			if (tooltipMode == "cityNames" && cityNamesDDL != "") {
				hexTooltip = findCityName(countLine, countColumn, cityNamesDDL);
			}
			
			///// tooltip: coordinates for each hex
			if (tooltipMode == "coordinatesForEachHex") {
				hexTooltip = "" + countColumn + ";" + countLine;
			}
			
			if (tooltipMode == "hexColorDescription") {
				hexTooltip = HEX_CAPTIONS[hexColorDescribes];
			}
			
			if (tooltipMode == "tileResource") {
				hexTooltip = generateTooltipResource(countColumn, countLine);
			}
			
			if (tooltipMode == "flipRisk") {
				hexTooltip = "";
				civsSpawn = [];
				civsRespawn = [];
				civsMinorFlip = [];
				
				// fill civsSpawn
				if (typeof MAP_MAJOR_FLIP[countColumn*1000+countLine] != "undefined") {
					civs = MAP_MAJOR_FLIP[countColumn*1000+countLine].slice(0,-1).split(';');
					civs.forEach(civ => {
						if (opponentsOfCivDDL == "") {
							civsSpawn[civsSpawn.length] = civ;
						} else {
							if (typeof PARTICIPANTS_VERSUS[opponentsOfCivDDL][civ] != "undefined" && (findSpawnYear(civ)>findSpawnYear(opponentsOfCivDDL))) {
								if (PARTICIPANTS_VERSUS[opponentsOfCivDDL][civ] != 0) {
									civsSpawn[civsSpawn.length] = civ;
								}
							}
						}
					});
				}
				
				// fill civsRespawn
				if (typeof MAP_MAJOR_RESPAWN_FLIP[countColumn*1000+countLine] != "undefined") {
					civs = MAP_MAJOR_RESPAWN_FLIP[countColumn*1000+countLine].slice(0,-1).split(';');
					civs.forEach(civ => {
						if (opponentsOfCivDDL == "") {
							civsRespawn[civsRespawn.length] = civ;
						} else {
							if (typeof PARTICIPANTS_VERSUS[opponentsOfCivDDL][civ] != "undefined" && PARTICIPANTS_VERSUS[opponentsOfCivDDL][civ] != 0) {
									civsRespawn[civsRespawn.length] = civ;
								}
						}
					});
				}
				
				// fill minor civs
				if (typeof MAP_MINOR_FLIP[countColumn*1000+countLine] != "undefined") {
					civs = MAP_MINOR_FLIP[countColumn*1000+countLine].slice(0,-1).split(';');
					civs.forEach(civ => {
						if (opponentsOfCivDDL == "") {
							civsMinorFlip[civsMinorFlip.length] = civ;
						} else {
							// civ is the minor civ potentially spawning on current tile
							civNo = MINOR_CIVS_NUMBER[civ];
							if (typeof MINOR_CIVS[civNo]["NotOverlap"] == "undefined" || MINOR_CIVS[civNo]["NotOverlap"] != opponentsOfCivDDL) {
								civsMinorFlip[civsMinorFlip.length] = civ;
							}
						}
					});
				}
				
				// build tooltip
				if (civsSpawn.length > 0) {
					hexTooltip = "SPAWN: ";
					i=0;
					do {
						hexTooltip += RhyeCivNameToReadableName(civsSpawn[i]);
						i++;
						if (i<civsSpawn.length) {
							hexTooltip += ", ";
						}
					} while (i<civsSpawn.length);
				}
				if (civsRespawn.length > 0) {
					if (hexTooltip.length > 0) {
						hexTooltip += "<br>";
					}
					hexTooltip += "RESPAWN: ";
					i=0;
					do {
						hexTooltip += RhyeCivNameToReadableName(civsRespawn[i]);
						i++;
						if (i<civsRespawn.length) {
							hexTooltip += ", ";
						}
					} while (i<civsRespawn.length);
				}
				if (civsMinorFlip.length > 0) {
					if (hexTooltip.length > 0) {
						hexTooltip += "<br>";
					}
					hexTooltip += "MINOR CIVS: ";
					i=0;
					do {
						civ = civsMinorFlip[i];
						civNo = MINOR_CIVS_NUMBER[civ];
						minYear = TURN_TO_YEAR[MINOR_CIVS[civNo]["MinTurn"]];
						maxYear = TURN_TO_YEAR[MINOR_CIVS[civNo]["MaxTurn"]];
						hexTooltip += RhyeMinorCivNameToReadableName(civ) + " (" + minYear + " - " + maxYear + ")";
						i++;
						if (i<civsMinorFlip.length) {
							hexTooltip += ", ";
						}
					} while (i<civsMinorFlip.length);
				}
			}
			
			// TODO: something better than replace all spaces by &nbsp; to force more words per line (e.g. on Rio de Janeiro with menu on 1980 screen, not totally readable because no multiline)
			hexTooltip = hexTooltip.split(' ').join('&nbsp;');
			
			///// --- END COLOR TILE & TOOLTIP ---
			
			
			
			///// RIVERS
			
			if (rivers[2] == "1") {
				svgRivers += SVG_river_E(tile_x0, tile_y0);
			}
			
			if (rivers[0] == "1") {
				svgRivers += SVG_river_SW(tile_x0, tile_y0);
			}
			
			if (rivers[1] == "1") {
				svgRivers += SVG_river_SE(tile_x0, tile_y0);
			}
			
			///// END OF RIVERS
			
			
			
			///// OVERLAY ICONS
			
			if (displayGraphicalOverlay) {
				if (MAP[countLine][countColumn][4] != 0) {
					// map: resource
					displayIconOverlay = 'resource_'+MAP[countLine][countColumn][4];
				} else if (typeof MAP_COLUMBIAN_EXCHANGE[countColumn*1000+countLine] != "undefined") {
					// RhyeDB: resource that appears later
					displayIconOverlay = 'resource_'+MAP_COLUMBIAN_EXCHANGE[countColumn*1000+countLine]["resource"];
				} else if (elevation == "m") {
					displayIconOverlay = 'terrain_mountain';
					if (tooltipMode == "hexColorDescription") {
					}
				} else if (feature1 == "JUNGLE") {
					displayIconOverlay = 'terrain_jungle';
				} else if (feature1 == "FOREST") {
					displayIconOverlay = 'terrain_forest';
				}
			}
			
			///// END OF OVERLAY ICONS
			
			
			
			///// ADD SVG ELEMENTS TO MAP
			if (displayIconOverlay == "") {
				svgMap += '<g>';
			} else {
				svgMap += '<g class="iconOverlay_' + displayIconOverlay.toLowerCase() + '">';
				svgMap += SVG_polygon(tile_x0, tile_y0, hexColor, displayHexGrid, hexTooltip); // 2 polygons if there is an overlay!
			}
			svgMap += SVG_polygon(tile_x0, tile_y0, hexColor, displayHexGrid, hexTooltip);
			svgMap += '</g>';
		}
	}
	svgMap += '<g class="rivers">' + svgRivers + '</g>';
	svgMap += "</svg>";
	MAP_BODY.innerHTML = "" + svgMap;
	
	tooltipTriggers = document.querySelectorAll('polygon');
	Array.from(tooltipTriggers).map(trigger => {
		trigger.addEventListener('mousemove', showTooltip);
		trigger.addEventListener('mouseout', hideTooltip);
	});
}
