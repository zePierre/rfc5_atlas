# RFC5 visualized - The Atlas for Rhye's and Fall of Civilization V

Developed by PiR / zePierre as an addin for Civ 5 Rhye's mod

[Latest version available on Gitlab](https://gitlab.com/zePierre/rfc5_atlas)

# HOW TO USE
*Just open Atlas.html*

If outdated, go to "generator" folder and run "generate_all.cmd" (requires python and lua installed on your machine and included in your OS path)
Compatibility tested with:
- Windows 10: Firefox, Edge

# OBJECTIVES
The objectives of this add-in are:
- to *clearly present the map mechanics of the mod*, such spawn/respawn zones, stability tile per tile, terrain, etc.
> "As a huge fan of RFC for Civilization IV, I always checked the stability maps and flip zones in the atlas before planning new cities." - PiR
- to be very accessible to RFC players, without needing any technical installations or knowledge
- to be 99.9% automated from the other files of the mod, so that no additional data would have to be maintained just for it
- not to alter the mod at all

# FEATURES
This tool can display the following information, based on mod files (located in Data, XML and Maps folders of the mod):
- terrain data, including resources, natural wonders and rivers
- starting locations for each civ (with names, spawn turns and years)
- minor civs with their capital and City-States
- the flip risk on each tile (in case of a later civ starting or any civ respawning)
- depending on a civ selection:
    - the stability map, flip area, and respawn flip area
    - city names
    - opponents that the civ might encounter
- technical coordinates for each tile
- saving the map with the above parameters as an image file (.svg)

## Roadmap / planned / todo
- sort Civilizations in Victory menu: review initVictoryMenu() function
- (minor) fix tooltips when displaying both City-States and Starting Locations on the same tooltip

## Out of scope:
- settlers map AI preference (seems unnecessary to display)
- display better names for natural wonders not renamed in XML/terrains.xml (no easy solution to access game files)

# THANKS
- To Rhye & all who contributed in the RFC mods and submods for the different versions of civs
- To the authors of [civ5map-tools](https://github.com/fechan/civ5map-tools)

# TECHNICAL DETAILS
Anyone with a browser can use this addin: only HTML, CSS and JS are required, without any library. An Internet connection is not necessary, except for fonts. However, each time the mod is updated, some JS files need to be regenerated.

The structure is as follows:
- The html file is located at the root of the mod folder for easy access to the user.
- The "resources" folder contains the Javascript logic for building the map and images. ** The file "resources/map_builder.js" is the main file of the addin beside the generator files. It contains harcoded data at its beginning.**
- The generator scripts are in a subfolder "generator" for easy access to the other resource files (mod files) via relative paths. **That is why the whole addin is designed to be a subfolder of the mod.**
- The generator scripts will write files in the "generated_js" folder, which contain data are not meant to be edited.
- A single html file is also generated: it contains all textual resources (.html/.js files and CSS code). This file can easily be copied to any location within the computer or towards another device. However, without the relative path towards the images, it will require and Internet connection to load the images.

The generator works as follows:
- There are Python scripts and LUA scripts. All can be run in proper order via generator/generate_all.cmd (which has been tested only on Windows 10 but should work on any OS, as long as lua and python are in the PATH).
- A Python script "civ5map_generate_js.py" generates the map.js file from the map file. **If the map file from the mod is renamed, the file generator/path_to_map.py needs to be updated accordingly.** This is where the only required module is used (kaitaistruct). Another Python script (required_modules.py) is first used in generate_all.cmd to ensure this module is installed.
- Other Python scripts read XML files for renaming
- LUA scripts include RhyeDB lua files and this way get their variables, to output them into JS files (or intermediary .TXT files for Python scripts). These JS files contain the raw data. It was decided to generate JS and not a better format for the data (like json for example), to allow users to open the HTML file locally, which would not be possible with JS requests on local files without altering the browser's security settings. **This tool, in its current state, requires that RhyeDB files do not print anything, unless as JS comments (lines starting by //).**

# HOW TO UPDATE THE ATLAS FROM A NEW MOD VERSION
The scripts should handle most content modifications from the mod, but may require adjustment depending on the updates made by Rhye. This is how to update the Atlas with a Windows 10 already properly configured (Git installed, Python, etc.) :
- Download & unzip Rhye's new mod version
- In the folder, open a git bash and enter 'git clone git@gitlab.com:zePierre/rfc5_atlas.git'
- Open a DOS prompt and go into the subfolder 'cd .\rfc5_atlas\generator'
- Launch generate_all.cmd
- Handle the errors and fix them
    - Open '.\rfc5_atlas\generator\path_to_map.py' in a text editor and check the file name in there versus what is located in 'Rhye's and Fall (v 90)\Maps'
    - Check if new buildings need to be added at the beginning of 'generator/minor_civs.lua'
	- If there is a new civ, you will need to add its icon into the folder 'resources', from MOD/Art/IconAtlas45.dds
    - Read the errors, improvise and contact me if necessary :)
- When you have no error launching generate_all.cmd open Atlas.html and check the features:
    - tooltips
	- hexes
	- settler advisor
	- city states "play all"
- Once assured there is no regression **remember to commit & push your changes!** 
    - Check with 'git status' which files you have modified, and review them with 'git diff' before committing (no need to review generated_js folder and single_html.html)
    - Commit 'git commit -am "Update from RnFXX"' (change the *XX* with the version!) and push 'git push'
- Zip 'rfc5_atlas' folder, make sure to remove '__pycache__', '.git' folders and '.gitlab-ci.yml', '.gitignore' files, then provide the zip file to Rhye for inclusion in the mod or publish it on https://forums.civfanatics.com/threads/the-atlas.671348/

# VERSION LOG

## v0.1 (23/03/21)
- POC (reading the map file to get its terrain data
- display in html with svg icons, zoom buttons
- civ selector for spawn zone
- hardcoded data about spawn zone in America
- hardcoded data about city name in America)

## v0.2 (26/03/21)
- fixed issue: the hexes of the bottom line should be more to the right than their line up (did the opposite)
- integrated into the mod folder

## v0.3 (09/04/21)
- added mountains; transforms file RhyeCityNamesDB.lua into js to display city name
- display city name according to JS DB
- fixed graphical issue (maps with even number of lines)

## v0.4 (10/04/21)
- multiple map modes can be displayed altogether (city names of a civ
- stability of a civ
- spawn & convert zone
- respawn & convert zone) + dynamic list of civs is read from RhyeMajorsDB.lua: the map can now display chosen civ's city names and new map mode "all spawn tiles" displays a map with 1 tile spawn for each civ (displaying civ name and date as tile's tooltip)

## v0.5 (11/04/21)
- view "flip area" and "respawn flip area" for each civ (so far not taking into account AdditionalPlots/SubtractPlots)

## v0.6 (19/04/21)
- changed name from RFC5_Visualized into Atlas
- added management of AdditionalPlots/SubtractPlots for flip areas

## v1.0 (20/04/21)
- added stability map
- captions popup & finished first full version

## v1.1 (01/05/21)
- move to gitlab
- fixed stability & flip maps (1 hex wrong) by building the map from bottom to top and accept missing maps
- reworked UI
- added marshes + jungles + flood plains + natural wonders + rivers in terrain map
- added secondary starts in starting location (previously called spawn tiles)
- added coordinates on map
- generation of a single html file containing all html/css/js/svg including data and enabling more options: enable/disable hex grid (but captions popup doesn't work in single html)

## v1.2 (03/05/21)
- removed html&css display of the map to replace it by a single svg image
- enable download of image as .svg

## v1.3 (04/07/21)
- UI entirely reworked to match game interface
- improved performance
- tooltips:
    - captions are now a tooltip choice
    - faster tooltips (only when svg is viewed within html but not as downloaded image)
    - new tooltip that displays if tile is in spawn/respawn flip zone and if so of which civs (also depending on civ opponents selection)
    - new tooltip for resources
- display names of
    - natural wonders from XML/terrains.xml
    - civs from XML/texts.xml
	- minor civs from XML/minorCivs.xml
- download file proposed name
- images displayed in single html
- add turns when years are displayed in starting locations
- water flip in darker red than land flip
- order civs by turn spawn in DDL
- showing which opponents are major or minor depending on your civ selection (from the "participants" file)
- manage oasis
- display resources and a few terrain features (mountains, jungles, forests) with images icons
- minor civs' locations: capitals and City-States, with a slider and an animation to watch them Rhye's
- the Settler Advisor: shortcuts with civs logo to prefill all 3 drop down lists, with stability map of selected civ and flip risk tooltip

## v1.4 (01/08/21)
- Update from RnF80

## v1.5 (09/01/23)
- Update from RnF88

## v1.6 (03/03/23)
- Update from RnF90
- Added section to the Readme: "HOW TO UPDATE THE ATLAS FROM A NEW MOD VERSION"
- Considered city name map done (was still in todo)

## v1.7 (03/06/24)
- Update from RnF95
- UI fixes
- victory menu contains Role Play Victory short & long descriptions
