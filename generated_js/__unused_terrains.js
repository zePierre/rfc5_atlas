const NB_LINE = 19;
const NB_COLUMNS = 13;
const terrains = [
["S","S","S","S","S","S","C","C","C","C","C","C","O"],
["S","S","S","C","C","C","C","O","O","O","O","O","O"],
["S","S","C","T","T","C","C","C","O","O","O","O","O"],
["S","S","S","C","T","C","T","T","C","O","O","O","O"],
["S","S","C","G","T","S","T","T","C","O","O","O","O"],
["S","S","C","C","G","T","S","G","C","O","O","O","C"],
["S","C","C","C","C","C","C","C","O","O","O","C","G"],
["S","C","C","O","O","O","O","O","O","O","O","C","G"],
["C","O","O","O","O","O","O","O","O","C","C","C","G"],
["C","C","O","O","O","O","O","O","C","C","G","G","C"],
["O","O","O","O","O","O","O","C","G","G","G","C","C"],
["O","O","O","O","O","O","O","O","C","G","G","G","C"],
["O","O","O","O","O","O","O","C","G","G","G","C","G"],
["C","O","O","O","O","O","O","C","G","G","G","C","C"],
["C","O","O","O","O","O","O","C","C","C","C","C","G"],
["O","O","O","O","O","O","O","O","O","C","C","C","C"],
["O","O","O","O","O","O","O","O","O","C","C","G","G"],
["O","O","O","O","O","O","O","O","O","O","C","C","C"],
["O","O","O","O","O","O","O","O","O","O","O","O","C"],
];
