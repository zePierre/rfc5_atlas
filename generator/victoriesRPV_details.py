# Author zePierre
from xml.dom import minidom

file = minidom.parse("../../RPVictory/VictoryText.xml")
rows = file.getElementsByTagName("Row")

print("const VICTORIES_RPV = [];")
technicalNames = open("./generated_txt/list_major_civs.txt", "r").read().split(";")
tagNameStart = "TXT_KEY_RPV_"
tagNameEnd = ""
civNameStart = "CIVILIZATION_"

for row in rows:
    if row.hasAttribute("Tag"):
        tag = row.getAttribute("Tag")
        if tag.startswith(tagNameStart):
            civName = tag[len(tagNameStart):]
            civName = civName[:len(civName)-len(tagNameEnd)]
            
            if civName in technicalNames:
                txt = row.getElementsByTagName("Text")[0].firstChild.nodeValue
                txt = txt.strip()
                print("VICTORIES_RPV[\"" + civName + "\"] = \"" + txt + "\";")
