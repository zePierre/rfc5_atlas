dofile "./include_timeline.lua"
dofile ".\\..\\..\\Data\\RhyeMapInGame.lua"

print("COLUMBIAN_EXCHANGE_YEAR_STARTS_AT = " .. 1600 .. ";") -- currently COLUMBIAN_EXCHANGE_YEAR = ConvertYearToGameTurn( 1600 + math.random( 1, 30 ) )
print("MAP_COLUMBIAN_EXCHANGE = [];")

for i,v in pairs(ColumbianExchange) do
    print("MAP_COLUMBIAN_EXCHANGE[" .. v["x"]*1000+v["y"] .. "] = [];")
    print("MAP_COLUMBIAN_EXCHANGE[" .. v["x"]*1000+v["y"] .. "][\"resource\"] = \"" .. string.sub(v["resource"],10) .. "\";") -- remove "RESOURCE_"
    print("MAP_COLUMBIAN_EXCHANGE[" .. v["x"]*1000+v["y"] .. "][\"quantity\"] = " .. v["quantity"] .. ";")
end
