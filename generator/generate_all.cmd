python --version
lua -v
python required_modules.py
python civ5map_generate_js.py > ../generated_js/map.js
lua city_names.lua > ../generated_js/city_names.js
lua major_civs.lua > ../generated_js/major_civs.js
lua stability.lua > ../generated_js/stability.js
lua list_major_civs.lua > ./generated_txt/list_major_civs.txt
python rename_major_civs.py > ../generated_js/rename_major_civs.js
python rename_minor_civs.py > ../generated_js/rename_minor_civs.js
python rename_natural_wonders.py > ../generated_js/rename_natural_wonders.js
lua participants.lua > ../generated_js/participants.js
lua columbian_exchange.lua > ../generated_js/columbian_exchange.js
lua minor_civs.lua > ../generated_js/minor_civs.js
lua timeline.lua > ../generated_js/timeline.js
python victoriesRPV_details.py > ../generated_js/victoriesRPV_details.js
python single_html.py > ../single_html.html
