# Author zePierre
from xml.dom import minidom

file = minidom.parse("../../XML/texts.xml")
wheres = file.getElementsByTagName("Where")

print("const MAJOR_CIVS_RENAME = [];")
technicalNames = open("./generated_txt/list_major_civs.txt", "r").read().split(";")
tagNameStart = "TXT_KEY_CIV5_DAWN_"
tagNameEnd = "_TITLE"
civNameStart = "CIVILIZATION_"
civNames = []

for technicalName in technicalNames:
    civNames.append(technicalName[len(civNameStart):])

for where in wheres:
    if where.hasAttribute("Tag"):
        tag = where.getAttribute("Tag")
        if tag.endswith(tagNameEnd) and tag.startswith(tagNameStart):
            civName = tag[len(tagNameStart):]
            civName = civName[:len(civName)-len(tagNameEnd)]
            if civName in civNames:
                txt = where.parentNode.getElementsByTagName("Set")[0].getElementsByTagName("Text")[0].firstChild.nodeValue
                txt = txt.strip()
                print("MAJOR_CIVS_RENAME[\"" + civNameStart + civName + "\"] = \"" + txt + "\";")
