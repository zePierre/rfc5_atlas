dofile ".\\..\\..\\Data\\RhyeMajorsDB.lua"
dofile "./include_timeline.lua"
dofile ".\\..\\..\\Data\\RhyeStabilityMapDB.lua"

print("const MAJOR_CIVS = [];")
i = 0
print("const MAJOR_CIVS_NUMBER = [];")

print("const MAP_START_CIV = [];")
print("const MAP_SECONDARY_START_CIV = [];")
print("const MAP_FlipAreaAdditionalPlots = [];")
print("const MAP_FlipAreaSubtractPlots = [];")
print("const MAP_RespawnFlipAreaAdditionalPlots = [];")
print("const MAP_RespawnFlipAreaSubtractPlots = [];")
initializedStartX = {}
initializedSecondaryStartX = {}

print("const MAP_MAJOR_FLIP = [];")
MAP_FLIP = {}

print("const MAP_MAJOR_RESPAWN_FLIP = [];")
MAP_RESPAWN_FLIP = {}

for nameCiv,civ in pairs(RhyeMajorCivs) do
	print("MAJOR_CIVS["..i.."] = [];")
	print("MAJOR_CIVS["..i.."][\"JSname\"] = \""..nameCiv.."\";")
	
	print("MAP_FlipAreaAdditionalPlots[\""..nameCiv.."\"] = [];")
	initializedStartXFlipAdd = {}
	print("MAP_FlipAreaSubtractPlots[\""..nameCiv.."\"] = [];")
	initializedStartXFlipSub = {}
	print("MAP_RespawnFlipAreaAdditionalPlots[\""..nameCiv.."\"] = [];")
	initializedStartXRespawnFlipAdd = {}
	print("MAP_RespawnFlipAreaSubtractPlots[\""..nameCiv.."\"] = [];")
	initializedStartXRespawnFlipSub = {}
	
	for attributeName,attributeValue in pairs(civ) do 
		if type(attributeValue) == "string" then
			print("MAJOR_CIVS["..i.."][\""..attributeName.."\"]=\""..attributeValue.."\";")
		elseif type(attributeValue) == "number" then
			print("MAJOR_CIVS["..i.."][\""..attributeName.."\"]="..attributeValue..";")
		elseif type(attributeValue) == "boolean" then
			if (attributeValue) then
				print("MAJOR_CIVS["..i.."][\""..attributeName.."\"]=true;")
			else
				print("MAJOR_CIVS["..i.."][\""..attributeName.."\"]=false;")
			end
		elseif type(attributeValue) == "table" then
			--print("MAJOR_CIVS["..i.."][\""..attributeName.."\"]=[];")
			if attributeName == "FlipAreaAdditionalPlots" or attributeName == "FlipAreaSubtractPlots" or attributeName == "RespawnFlipAreaAdditionalPlots" or attributeName == "RespawnFlipAreaSubtractPlots" then
				for n1,coordinates in pairs(attributeValue) do 
					if attributeName == "FlipAreaAdditionalPlots" then
						if (initializedStartXFlipAdd[coordinates[1]] == nil) then
							print("MAP_FlipAreaAdditionalPlots[\""..nameCiv.."\"]["..coordinates[1].."] = [];")
							initializedStartXFlipAdd[coordinates[1]] = true
						end
						print("MAP_FlipAreaAdditionalPlots[\""..nameCiv.."\"]["..coordinates[1].."]["..coordinates[2].."] = true;")
						
					elseif attributeName == "FlipAreaSubtractPlots" then
						if (initializedStartXFlipSub[coordinates[1]] == nil) then
							print("MAP_FlipAreaSubtractPlots[\""..nameCiv.."\"]["..coordinates[1].."] = [];")
							initializedStartXFlipSub[coordinates[1]] = true
						end
						print("MAP_FlipAreaSubtractPlots[\""..nameCiv.."\"]["..coordinates[1].."]["..coordinates[2].."] = true;")
						
					elseif attributeName == "RespawnFlipAreaAdditionalPlots" then
						if (initializedStartXRespawnFlipAdd[coordinates[1]] == nil) then
							print("MAP_RespawnFlipAreaAdditionalPlots[\""..nameCiv.."\"]["..coordinates[1].."] = [];")
							initializedStartXRespawnFlipAdd[coordinates[1]] = true
						end
						print("MAP_RespawnFlipAreaAdditionalPlots[\""..nameCiv.."\"]["..coordinates[1].."]["..coordinates[2].."] = true;")
						
					elseif attributeName == "RespawnFlipAreaSubtractPlots" then
						if (initializedStartXRespawnFlipSub[coordinates[1]] == nil) then
							print("MAP_RespawnFlipAreaSubtractPlots[\""..nameCiv.."\"]["..coordinates[1].."] = [];")
							initializedStartXRespawnFlipSub[coordinates[1]] = true
						end
						print("MAP_RespawnFlipAreaSubtractPlots[\""..nameCiv.."\"]["..coordinates[1].."]["..coordinates[2].."] = true;")
					end
				end
			else
				--print("// table "..attributeName.." ignored")
			end
		else
			--print("// "..attributeName.." ignored ("..type(attributeValue)..")")
		end
	end
	
	if (initializedStartX[civ["StartX"]] == nil) then
		print("MAP_START_CIV["..civ["StartX"].."] = [];")
		initializedStartX[civ["StartX"]] = true
	end
	
	if (initializedSecondaryStartX[civ["SecondaryStartX"]] == nil and civ["SecondaryStartX"] ~= nil) then
		print("MAP_SECONDARY_START_CIV["..civ["SecondaryStartX"].."] = [];")
		initializedSecondaryStartX[civ["SecondaryStartX"]] = true
	end
	
	print("MAP_START_CIV["..civ["StartX"].."]["..civ["StartY"].."] = \""..nameCiv.."\";")
	if (civ["SecondaryStartX"] ~= nil and civ["SecondaryStartY"] ~= nil) then
		print("MAP_SECONDARY_START_CIV["..civ["SecondaryStartX"].."]["..civ["SecondaryStartY"].."] = \""..nameCiv.."\";")
	end
	
	
	print("MAJOR_CIVS["..i.."][\"spawnTurn\"] = \""..ConvertYearToGameTurn(civ["SpawnYear"]).."\";")
	
	
	print("MAJOR_CIVS_NUMBER[\""..nameCiv.."\"] = "..i..";")
	-- fill MAP_FLIP
	for x=civ["FlipAreaWestX"], civ["FlipAreaEastX"], 1 do
		for y=civ["FlipAreaSouthY"], civ["FlipAreaNorthY"], 1 do
			notInSubtractPlots = true
			if (civ["FlipAreaSubtractPlots"] ~= nil) then
				for k,coord in pairs(civ["FlipAreaSubtractPlots"]) do
					if civ["FlipAreaSubtractPlots"][1] == x and civ["FlipAreaSubtractPlots"][2] == y then
						notInSubtractPlots = false
					end
				end
			end
			if (notInSubtractPlots) then
				if (MAP_FLIP[x*1000+y] ~= nil) then
					MAP_FLIP[x*1000+y] = MAP_FLIP[x*1000+y] .. nameCiv .. ";"
				else
					MAP_FLIP[x*1000+y] = nameCiv .. ";"
				end
			end
		end
	end
	if (civ["FlipAreaAdditionalPlots"] ~= nil) then
		for k,coord in pairs(civ["FlipAreaAdditionalPlots"]) do
			if (MAP_FLIP[coord[1]*1000+coord[2]] ~= nil) then
				MAP_FLIP[coord[1]*1000+coord[2]] = MAP_FLIP[coord[1]*1000+coord[2]] .. nameCiv .. ";"
			else
				MAP_FLIP[coord[1]*1000+coord[2]] = nameCiv .. ";"
			end
		end
	end
	
	-- fill MAP_RESPAWN_FLIP
	if (civ["RespawnFlipAreaWestX"] ~= nil) then
		for x=civ["RespawnFlipAreaWestX"], civ["RespawnFlipAreaEastX"], 1 do
			for y=civ["RespawnFlipAreaSouthY"], civ["RespawnFlipAreaNorthY"], 1 do
				notInSubtractPlots = true
				if (civ["RespawnFlipAreaSubtractPlots"] ~= nil) then
					for k,coord in pairs(civ["RespawnFlipAreaSubtractPlots"]) do
						if civ["RespawnFlipAreaSubtractPlots"][1] == x and civ["RespawnFlipAreaSubtractPlots"][2] == y then
							notInSubtractPlots = false
						end
					end
				end
				if (notInSubtractPlots) then
					if (MAP_RESPAWN_FLIP[x*1000+y] ~= nil) then
						MAP_RESPAWN_FLIP[x*1000+y] = MAP_RESPAWN_FLIP[x*1000+y] .. nameCiv .. ";"
					else
						MAP_RESPAWN_FLIP[x*1000+y] = nameCiv .. ";"
					end
				end
			end
		end
		if (civ["RespawnFlipAreaAdditionalPlots"] ~= nil) then
			for k,coord in pairs(civ["RespawnFlipAreaAdditionalPlots"]) do
				if (MAP_RESPAWN_FLIP[coord[1]*1000+coord[2]] ~= nil) then
					MAP_RESPAWN_FLIP[coord[1]*1000+coord[2]] = MAP_RESPAWN_FLIP[coord[1]*1000+coord[2]] .. nameCiv .. ";"
				else
					MAP_RESPAWN_FLIP[coord[1]*1000+coord[2]] = nameCiv .. ";"
				end
			end
		end
	end
	
	i = i+1
end

for coord,civs in pairs(MAP_FLIP) do
	print("MAP_MAJOR_FLIP["..coord.."] = \""..civs.."\";")
end

for coord,civs in pairs(MAP_RESPAWN_FLIP) do
	print("MAP_MAJOR_RESPAWN_FLIP["..coord.."] = \""..civs.."\";")
end
