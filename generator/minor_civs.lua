dofile "./include_timeline.lua"

GameInfo = {
	Buildings = {
		BUILDING_STELE = { ID = "STELE" },
		BUILDING_WAT = { ID = "BUILDING_WAT" },
		BUILDING_CANDI = { ID = "BUILDING_CANDI" },
		BUILDING_MAYA_PYRAMID = { ID = "BUILDING_MAYA_PYRAMID" },
		BUILDING_CEILIDH_HALL = { ID = "BUILDING_CEILIDH_HALL" },
		BUILDING_DUCAL_STABLE = { ID = "BUILDING_DUCAL_STABLE" },
		BUILDING_MUD_PYRAMID_MOSQUE = { ID = "BUILDING_MUD_PYRAMID_MOSQUE" },
		BUILDING_IKANDA = { ID = "BUILDING_IKANDA" },
		BUILDING_PALACE = { ID = "BUILDING_PALACE" },
	}
}

dofile ".\\..\\..\\Data\\RhyeMinorsDB.lua"

print("MINOR_CIVS = [];")
print("const MAP_MINOR_FLIP = [];")
MAP_FLIP = {}

i = 0
print("const MINOR_CIVS_NUMBER = [];")
print("const MAP_MINOR_CIVS_CAPITALS = [];")

for minorCivTechnicalName, minorCiv in pairs(RhyeMinorCivs) do
	print("MINOR_CIVS["..i.."] = [];")
	print("MINOR_CIVS["..i.."][\"JSname\"] = \"" .. minorCivTechnicalName .. "\";")
	
	if minorCiv["MinTurn"] ~= nil then
		print("MINOR_CIVS["..i.."][\"MinTurn\"] = " .. minorCiv["MinTurn"] .. ";")
		print("MINOR_CIVS["..i.."][\"MaxTurn\"] = " .. minorCiv["MaxTurn"] .. ";")
	end
	
	if minorCiv["FlipAreaWestX"] ~= nil then
		print("MINOR_CIVS["..i.."][\"FlipAreaWestX\"] = " .. minorCiv["FlipAreaWestX"] .. ";")
		print("MINOR_CIVS["..i.."][\"FlipAreaSouthY\"] = " .. minorCiv["FlipAreaSouthY"] .. ";")
		print("MINOR_CIVS["..i.."][\"FlipAreaEastX\"] = " .. minorCiv["FlipAreaEastX"] .. ";")
		print("MINOR_CIVS["..i.."][\"FlipAreaNorthY\"] = " .. minorCiv["FlipAreaNorthY"] .. ";")
	end
	
	if minorCiv["FallLimitWestX"] ~= nil then
		print("MINOR_CIVS["..i.."][\"FallLimitWestX\"] = " .. minorCiv["FallLimitWestX"] .. ";")
		print("MINOR_CIVS["..i.."][\"FallLimitSouthY\"] = " .. minorCiv["FallLimitSouthY"] .. ";")
		print("MINOR_CIVS["..i.."][\"FallLimitEastX\"] = " .. minorCiv["FallLimitEastX"] .. ";")
		print("MINOR_CIVS["..i.."][\"FallLimitNorthY\"] = " .. minorCiv["FallLimitNorthY"] .. ";")
	end
	
	if minorCiv["MinCityFlips"] ~= nil then
		print("MINOR_CIVS["..i.."][\"MinCityFlips\"] = " .. minorCiv["MinCityFlips"] .. ";")
		print("MINOR_CIVS["..i.."][\"MaxCityFlips\"] = " .. minorCiv["MaxCityFlips"] .. ";")
	end
	
	if minorCiv["HostileVs"] ~= nil then
		hostileVs = ""
		for k,v in pairs(minorCiv["HostileVs"]) do
			hostileVs = hostileVs .. v .. ";"
		end
		print("MINOR_CIVS["..i.."][\"HostileVs\"] = \"" .. hostileVs .. "\";")
	end
	
	if minorCiv["HostileVsAll"] ~= nil and minorCiv["HostileVsAll"] then
		print("MINOR_CIVS["..i.."][\"HostileVsAll\"] = true;")
	else
		print("MINOR_CIVS["..i.."][\"HostileVsAll\"] = false;")
	end
	
	if minorCiv["NotOverlap"] ~= nil then
		print("MINOR_CIVS["..i.."][\"NotOverlap\"] = \"" .. minorCiv["NotOverlap"] .. "\";")
	end
	
	if minorCiv["CapitalX"] ~= nil then
		print("MINOR_CIVS["..i.."][\"CapitalX\"] = " .. minorCiv["CapitalX"] .. ";")
		print("MINOR_CIVS["..i.."][\"CapitalY\"] = " .. minorCiv["CapitalY"] .. ";")
		print("MINOR_CIVS["..i.."][\"CapitalName\"] = \"" .. minorCiv["CapitalName"] .. "\";")
		coordinates = (minorCiv["CapitalX"]*1000+minorCiv["CapitalY"])
		print("MAP_MINOR_CIVS_CAPITALS[" .. coordinates .. "] = \"" .. minorCivTechnicalName .. "\";")
	end
	
	print("MINOR_CIVS_NUMBER[\""..minorCivTechnicalName.."\"] = "..i..";")
	
	-- fill MAP_FLIP
	if (minorCiv["FlipAreaWestX"] ~= nil) then
		for x=minorCiv["FlipAreaWestX"], minorCiv["FlipAreaEastX"], 1 do
			for y=minorCiv["FlipAreaSouthY"], minorCiv["FlipAreaNorthY"], 1 do
				if (MAP_FLIP[x*1000+y] ~= nil) then
					MAP_FLIP[x*1000+y] = MAP_FLIP[x*1000+y] .. minorCivTechnicalName .. ";"
				else
					MAP_FLIP[x*1000+y] = minorCivTechnicalName .. ";"
				end
			end
		end
	end
	
	i = i + 1
	
	print("")
end

print("")
print("")
print("")

for coord,civs in pairs(MAP_FLIP) do
	print("MAP_MINOR_FLIP["..coord.."] = \""..civs.."\";")
end

print("")
print("")
print("")

print("MAP_CITY_STATES = [];")
StartTurn_ExhaustiveList = {}

for i, cityState in pairs(RhyeCityStates) do
	coordinates = cityState["StartX"]*1000+cityState["StartY"]
	print("MAP_CITY_STATES["..coordinates.."] = [];")
	print("MAP_CITY_STATES["..coordinates.."][\"CivType\"] = \"" .. cityState["CivType"] .. "\";")
	print("MAP_CITY_STATES["..coordinates.."][\"CityName\"] = \"" .. cityState["CityName"] .. "\";")
	
	if (cityState["MajorCivPaired"] ~= nil) then
		print("MAP_CITY_STATES["..coordinates.."][\"MajorCivPaired\"] = \"" .. cityState["MajorCivPaired"] .. "\";") --always spawning when this major civ is in the game
	end
	
	eras = ""
	
	for k,v in pairs(cityState["IsInEra"]) do
		eras = eras .. v
	end
	
	print("MAP_CITY_STATES["..coordinates.."][\"eras\"] = \"" .. eras .. "\";")
	print("MAP_CITY_STATES["..coordinates.."][\"StartTurn\"] = " .. cityState["StartTurn"] .. ";")
	StartTurn_ExhaustiveList[#StartTurn_ExhaustiveList+1] = cityState["StartTurn"]
	
	if cityState["MajorCivReplacement"] ~= nil then
		print("MAP_CITY_STATES["..coordinates.."][\"MajorCivReplacement\"] = \"" .. cityState["MajorCivReplacement"] .. "\";")
	end
	if cityState["MajorCivReplacement2"] ~= nil then
		print("MAP_CITY_STATES["..coordinates.."][\"MajorCivReplacement2\"] = \"" .. cityState["MajorCivReplacement2"] .. "\";")
	end
	
	print("")
end

local ListChecked = {}
local TIMELINE_CITY_STATES = {}
for _,v in ipairs(StartTurn_ExhaustiveList) do
   if (not ListChecked[v]) then
       TIMELINE_CITY_STATES[#TIMELINE_CITY_STATES+1] = v
       ListChecked[v] = true
   end
end
table.sort(TIMELINE_CITY_STATES)
print("TIMELINE_CITY_STATES = [];")
for k,v in pairs(TIMELINE_CITY_STATES) do
	print("TIMELINE_CITY_STATES["..k.."] = "..v..";")
end
