from path_to_map import path_to_map

import civ5map
map = civ5map.Civ5map.from_file(path_to_map())

print("const MAP_NB_LINE = " + str(map.header.height) + ";")
print("const MAP_NB_COLUMNS = " + str(map.header.width) + ";")

print("const MAP = [")

y=0
for row in map.mapdata.plot_matrix:
    x=0
    # each line is an array of plots
    print("[")
    for plot in row.plot_list:
        # each plot is an array of plot properties
        print("[", end='')
        
        
        
        # terrain
        print("\"" + map.header.terrain_list.values[plot.terrain_type_id][8] + "\",", end='')
        
        """ Possible values for terrain:
        TERRAIN_OCEAN
        TERRAIN_COAST
        TERRAIN_SNOW
        TERRAIN_TUNDRA
        TERRAIN_GRASS
        TERRAIN_PLAINS
        TERRAIN_DESERT
        """
        
        
        
        # elevation
        print("\"" + plot.elevation.name[0] + "\",", end='')
        
        """ Possible values for elevation:
        none
        hill
        mountain
        """
        
        
        
        # continent
        #print("\"" + plot.continent.name + "\",", end='')
        
        """ Possible values for continent:
        africa
        americas
        asia
        europe
        """
        
        
        
        # feature1
        if plot.feature1_type_id != 255:
            print("\"" + map.header.feature1_list.values[plot.feature1_type_id][8:] + "\",", end='')
        else:
            print("0,", end='')
        
        """ Possible values for feature1:
        0 FEATURE_ICE
        1 FEATURE_JUNGLE 
        2 FEATURE_MARSH 
        3 FEATURE_OASIS 
        4 FEATURE_FLOOD_PLAINS 
        5 FEATURE_FOREST 
        7 FEATURE_ATOLL
        """
        
        
        
        # feature2 (natural wonder)
        if plot.feature2_type_id != 255:
            print("\"" + map.header.feature2_list.values[plot.feature2_type_id][8:] + "\",", end='')
        else:
            print("0,", end='')
        
        """ Possible values for feature2:
        0 FEATURE_CRATER
        1 FEATURE_FUJI
        2 FEATURE_MESA
        3 FEATURE_REEF
        4 FEATURE_VOLCANO
        5 FEATURE_GIBRALTAR
        6 FEATURE_GEYSER
        7 FEATURE_FOUNTAIN_YOUTH
        8 FEATURE_POTOSI
        10 FEATURE_SRI_PADA
        11 FEATURE_MT_SINAI
        12 FEATURE_MT_KAILASH
        13 FEATURE_ULURU
        14 FEATURE_LAKE_VICTORIA
        15 FEATURE_KILIMANJARO
        17 FEATURE_VOLCANO_VESUVIUS
        18 FEATURE_VOLCANO_ETNA
        19 FEATURE_VOLCANO_MAUNAKEA
        20 FEATURE_VOLCANO_NEVADO
        21 FEATURE_VOLCANO_PELEE
        """
        
        
        # resource type
        if plot.resource_type_id != 255:
            print("\"" + map.header.resource_list.values[plot.resource_type_id][9:] + "\",", end='')
        else:
            print("0,", end='')
            
        """ Possible values for resource type:
        0 RESOURCE_IRON
        1 RESOURCE_HORSE
        2 RESOURCE_COAL
        3 RESOURCE_OIL
        4 RESOURCE_ALUMINUM
        5 RESOURCE_URANIUM
        6 RESOURCE_WHEAT
        7 RESOURCE_COW
        8 RESOURCE_SHEEP
        9 RESOURCE_DEER
        10 RESOURCE_BANANA
        11 RESOURCE_FISH
        12 RESOURCE_STONE
        13 RESOURCE_WHALE
        14 RESOURCE_PEARLS
        15 RESOURCE_GOLD
        16 RESOURCE_SILVER
        17 RESOURCE_GEMS
        18 RESOURCE_MARBLE
        19 RESOURCE_IVORY
        20 RESOURCE_FUR
        21 RESOURCE_DYE
        22 RESOURCE_SPICES
        23 RESOURCE_SILK
        24 RESOURCE_SUGAR
        25 RESOURCE_COTTON
        26 RESOURCE_WINE
        27 RESOURCE_INCENSE
        30 RESOURCE_COPPER
        31 RESOURCE_SALT
        32 RESOURCE_CRAB
        33 RESOURCE_TRUFFLES
        34 RESOURCE_CITRUS
        40 RESOURCE_BISON
        41 RESOURCE_COCOA
        42 RESOURCE_FA_RICE
        43 RESOURCE_FA_MAIZE
        44 RESOURCE_FA_TEA
        45 RESOURCE_FA_TOBACCO
        """
        
        
        
        # resource quantity
        #print("" + str(plot.resource_amount) + ",", end='')
        
        
        
        # river
        print("\"", end='')
        
        if plot.river.southwest_edge:
            print("1", end='')
        else:
            print("0", end='')
        
        if plot.river.southeast_edge:
            print("1", end='')
        else:
            print("0", end='')
        
        if plot.river.east_edge:
            print("1", end='')
        else:
            print("0", end='')
        
        print("\",", end='')
        
        
        
        # close the array of plot properties
        print("],", end='')
        
        # in case of debug, display coordinates
        #print("//" + str(x) + ";" + str(y), end='')
        
        print("")
        x=x+1
    # close the array of the line
    print("],")
    y=y+1
# close the MAP array
print("];")

#this is to be output into terrains.js
