# Author zePierre

# list natural wonders technical names from the map file
from path_to_map import path_to_map
import civ5map
map = civ5map.Civ5map.from_file(path_to_map())

technicalNames = []
for row in map.mapdata.plot_matrix:
    for plot in row.plot_list:
        # feature2 (natural wonder)
        if plot.feature2_type_id != 255:
            technicalNames.append(map.header.feature2_list.values[plot.feature2_type_id])


# look in XML/terrains.xml for the renaming of any of those natural wonders
from xml.dom import minidom

file = minidom.parse("../../XML/terrains.xml")

print("const NATURAL_WONDERS_RENAME = [];")

whereTags = file.getElementsByTagName("Where")

for whereTag in whereTags:
    if (whereTag.hasAttribute("Type")):
        if whereTag.getAttribute("Type") in technicalNames:
            rename = whereTag.parentNode.getElementsByTagName("Set")[0].getAttribute("Description")
            print("NATURAL_WONDERS_RENAME[\"" + whereTag.getAttribute("Type")[8:] + "\"] = \"" + rename + "\";")

typeTags = file.getElementsByTagName("Type")
for typeTag in typeTags:
    if (typeTag.firstChild.nodeValue in technicalNames):
        rename = typeTag.parentNode.getElementsByTagName("Description")[0].firstChild.nodeValue
        print("NATURAL_WONDERS_RENAME[\"" + typeTag.firstChild.nodeValue[8:] + "\"] = \"" + rename + "\";")
