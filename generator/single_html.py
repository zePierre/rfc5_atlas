# Author zePierre

# get js scripts
jsScripts = ""
jsScripts = jsScripts + open("../generated_js/map.js", "r").read()
jsScripts = jsScripts + open("../generated_js/city_names.js", "r").read()
jsScripts = jsScripts + open("../generated_js/major_civs.js", "r").read()
jsScripts = jsScripts + open("../generated_js/stability.js", "r").read()
jsScripts = jsScripts + open("../generated_js/rename_major_civs.js", "r").read()
jsScripts = jsScripts + open("../generated_js/rename_minor_civs.js", "r").read()
jsScripts = jsScripts + open("../generated_js/rename_natural_wonders.js", "r").read()
jsScripts = jsScripts + open("../generated_js/participants.js", "r").read()
jsScripts = jsScripts + open("../generated_js/columbian_exchange.js", "r").read()
jsScripts = jsScripts + open("../generated_js/minor_civs.js", "r").read()
jsScripts = jsScripts + open("../generated_js/timeline.js", "r").read()
jsScripts = jsScripts + open("../resources/map_builder.js", "r").read().replace("./resources/","https://gitlab.com/zePierre/rfc5_atlas/-/raw/master/resources/").replace(".png",".png?inline=false")

# optimize size by putting civ names into short variables
listMajorCivs = open("./generated_txt/list_major_civs.txt", "r").read().split(';')
jsReplaceCivNames = ""
i = 0
for civName in listMajorCivs:
    if (len(civName) > len("CIVILIZATION_")):
        i = i + 1
        var = "c" + str(i)
        jsReplaceCivNames = jsReplaceCivNames + var + "=\"" + civName + "\";"
        jsScripts = jsScripts.replace("\"" + civName + "\"", var)

# build file
singleFileTxt = ""
singleFileTxt = singleFileTxt + open("../Atlas.html", "r").read().split('</body>')[0].replace("./resources/","https://gitlab.com/zePierre/rfc5_atlas/-/raw/master/resources/").replace(".png",".png?inline=false")  + '<script>'
singleFileTxt = singleFileTxt + jsReplaceCivNames + jsScripts
singleFileTxt = singleFileTxt + '</script></body></html>'

# optimize size by changing variable names
singleFileTxt = singleFileTxt.replace("MAP_CITY_NAMES", "M1")
singleFileTxt = singleFileTxt.replace("MAJOR_CIVS", "M2")
singleFileTxt = singleFileTxt.replace("MAP_START_CIV", "M3")
singleFileTxt = singleFileTxt.replace("MAP_SECONDARY_START_CIV", "M4")
singleFileTxt = singleFileTxt.replace("MAP_FlipAreaAdditionalPlots", "M5")
singleFileTxt = singleFileTxt.replace("MAP_FlipAreaSubtractPlots", "M6")
singleFileTxt = singleFileTxt.replace("MAP_RespawnFlipAreaAdditionalPlots", "M7")
singleFileTxt = singleFileTxt.replace("MAP_RespawnFlipAreaSubtractPlots", "M8")
singleFileTxt = singleFileTxt.replace("MAP_Stability", "M9")
singleFileTxt = singleFileTxt.replace("MAP_Participants", "M10")

# print
print(singleFileTxt)
