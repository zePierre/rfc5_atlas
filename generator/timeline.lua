dofile "./include_timeline.lua"

print("TURN_TO_YEAR = [];")
i=0
while (i<TIMELINE_lastTurn) do
	year = TIMELINE_orderedTimeline[i]
	year = string.gsub(year, "BC", "-")
	year = string.gsub(year, "AD", "")
	print("TURN_TO_YEAR["..i.."] = " .. year .. ";")
	i=i+1
end
