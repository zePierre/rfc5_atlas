# Author zePierre
from xml.dom import minidom

file = minidom.parse("../../XML/minorCivs.xml")
rows = file.getElementsByTagName("MinorCivilizations")[0].getElementsByTagName("Row")

print("const MINOR_CIVS_RENAME = [];")

for row in rows:
    field_Type = row.getElementsByTagName("Type")[0].firstChild.nodeValue
    field_ShortDescription = row.getElementsByTagName("ShortDescription")[0].firstChild.nodeValue
    print("MINOR_CIVS_RENAME[\"" + field_Type + "\"] = \"" + field_ShortDescription + "\";")
