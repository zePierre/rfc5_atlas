dofile ".\\..\\..\\Data\\RhyeTimeline.lua"

TIMELINE_orderedTimeline = {}
TIMELINE_lastTurn = 0
for year,turn in pairs(Timeline) do
	TIMELINE_orderedTimeline[turn] = year
	TIMELINE_lastTurn = math.max(turn, TIMELINE_lastTurn)
end

-- look for value = gameTurn in table Timeline and return key, replacing "BC" by "-" and "AD" by ""
function ConvertYearToGameTurn(year)
	spawnTurn = -1
	currentTurn = 0
	while (spawnTurn == -1 and currentTurn < TIMELINE_lastTurn) do
		txt = TIMELINE_orderedTimeline[currentTurn]
		txt = string.gsub(txt, "BC", "-")
		txt = string.gsub(txt, "AD", "")
		timelineYear = tonumber(txt)
		
		-- if exact match OR we just passed the year
		if (timelineYear >= year) then
			spawnTurn = currentTurn
		end
		currentTurn = currentTurn +1
	end
	return spawnTurn
end
