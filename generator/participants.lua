dofile ".\\..\\..\\Data\\RhyeParticipants.lua"

print("const PARTICIPANTS_VERSUS = [];")
print("const PARTICIPANTS_LIST = [];")

listParticipants = {}
for nameCiv,participants in pairs(RhyeParticipants) do
	print("PARTICIPANTS_VERSUS[\""..nameCiv.."\"] = [];")
	for participant,value in pairs(participants) do 
		print("PARTICIPANTS_VERSUS[\""..nameCiv.."\"][\""..participant.."\"] = "..value..";")
		
		notInListParticipants = true
		nbParticipants = 0
		for key, value in pairs(listParticipants) do
			if value == participant then
				notInListParticipants = false
			end
			nbParticipants = nbParticipants +1
		end
		if (notInListParticipants) then
			table.insert(listParticipants, participant)
			print("PARTICIPANTS_LIST["..nbParticipants.."] = \""..participant.."\";")
		end
	end
end
