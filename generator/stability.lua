dofile ".\\..\\..\\Data\\RhyeStabilityMapDB.lua"

print("const MAP_Stability = [];")


for nameCiv,plots in pairs(PlotStability) do
	print("MAP_Stability[\""..nameCiv.."\"] = [];")
	for coordinates,stabilityValue in pairs(plots) do 
		print("MAP_Stability[\""..nameCiv.."\"]["..coordinates.."] = "..stabilityValue..";")
	end
end
