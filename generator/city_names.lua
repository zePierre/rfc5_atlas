dofile ".\\..\\..\\Data\\RhyeCityNamesDB.lua"

print("const MAP_CITY_NAMES = [];")
for nameCiv,civ in pairs(CityNamesMap) do 
	print("MAP_CITY_NAMES[\""..nameCiv.."\"] = [];")
	for cityName,coordinatesList in pairs(civ) do 
		for a,coordinates in pairs(coordinatesList) do 
			print("MAP_CITY_NAMES[\""..nameCiv.."\"]["..coordinates.."]=\""..cityName.."\";")
		end
	end
end
